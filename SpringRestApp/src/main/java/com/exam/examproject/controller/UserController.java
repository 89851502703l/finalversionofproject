package com.exam.examproject.controller;

import com.exam.examproject.auth.LoginRequestDto;
import com.exam.examproject.auth.RegistrationRequestDto;
import com.exam.examproject.auth.User;
import com.exam.examproject.exceptionHandling.UserDuplicateEntity;
import com.exam.examproject.exceptionHandling.UserDuplicateException;
import com.exam.examproject.servise.api.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Tag(name = "Пользователь", description = "API для авторизации и аунтификации")
@RestController
@RequestMapping("/api")
public class UserController {

    private final AuthenticationService authenticationService;

    @Autowired
    public UserController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    @Operation(summary = "Вход в систему")
    public void login(@RequestBody LoginRequestDto loginRequestDTO) {
        authenticationService.authorize(loginRequestDTO);
    }

    @PostMapping("/logout")
    @Operation(summary = "Выход из системы")
    public void logout() {
        authenticationService.logout();
    }


    @PostMapping("/register")
    @Operation(summary = "Регистрация")
    public User register(@RequestBody @Validated RegistrationRequestDto registrationRequestDTO) {
        return authenticationService.register(registrationRequestDTO);
    }

    @PostMapping("/current")
    @Operation(summary = "Получение пользователя в системе")
    public Principal current(Principal principal) {
        return principal;
    }


    @ExceptionHandler
    public ResponseEntity<UserDuplicateEntity> handleException (UserDuplicateException exception) {
        UserDuplicateEntity user = new UserDuplicateEntity();
        user.setInfo(exception.getMessage());
        return new ResponseEntity<>(user, HttpStatus.NOT_ACCEPTABLE);
    }
}
