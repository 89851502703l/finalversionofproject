package com.exam.examproject.controller;

import com.exam.examproject.dto.StudentDto;
import com.exam.examproject.dto.TeacherDto;
import com.exam.examproject.servise.api.TeacherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST контроллер для работы с преподавателями
 */

@Tag(name = "Преподаватель", description = "API для преподавателей")
@RestController
@RequestMapping("/teachers")
public class TeacherController {

    private TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping
    @Operation(summary = "Получение списка всех преподавателей")
    public List<TeacherDto> getAllTeachers() {
        return teacherService.getAllTeachers();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получение преподавателя по идентификатору")
    public TeacherDto getTeacherById(@PathVariable int id) {
        return teacherService.getTeacherById(id);
    }

    @PostMapping
    @Operation(summary = "Добавление учителя")
    public TeacherDto addTeacher(@RequestBody TeacherDto teacherDto) {
        return teacherService.addTeacher(teacherDto);
    }

    @PutMapping
    @Operation(summary = "Изменение преподавателя")
    public TeacherDto updateTeacher(@RequestBody TeacherDto teacherDto) {
        return teacherService.updateTeacher(teacherDto);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление преподавателя")
    public void deleteTeacher(@PathVariable int id) {
        teacherService.deleteTeacher(id);
    }

    @GetMapping("/teacher/{id}")
    @Operation(summary = "Получение студентов преподавателя по идентификатору")
    public List<StudentDto> getStudentsByTeacherId(@PathVariable int id) {
        return teacherService.getStudentsByTeacherId(id);
    }


}
