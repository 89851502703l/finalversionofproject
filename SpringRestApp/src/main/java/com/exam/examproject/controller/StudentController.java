package com.exam.examproject.controller;

import com.exam.examproject.dto.StudentDto;
import com.exam.examproject.dto.TeacherDto;
import com.exam.examproject.rpc.AggregatorService;
import com.exam.examproject.servise.api.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST контроллер для работы со студентами
 */

@Tag(name = "Студент", description = "API для студентов")
@RestController
@RequestMapping("/students")
public class StudentController {

    private StudentService studentService;
    private AggregatorService aggregatorService;

    @Autowired
    public StudentController(StudentService studentService, AggregatorService aggregatorService) {
        this.studentService = studentService;
        this.aggregatorService = aggregatorService;
    }


    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    @Operation(summary = "Получение списка всех студентов")
    public List<StudentDto> getAllStudents() {
        List<StudentDto> allStudents = studentService.getAllStudents();
        aggregatorService.remoteLog("Получен список студентов", 2);
        return allStudents;
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получение студента по идентификатору")
    public StudentDto getStudentById(@PathVariable int id) {
        StudentDto student = studentService.getStudentById(id);
        aggregatorService.remoteLog("Получен студент", 2);
        return student;
    }

    @PostMapping
    @Operation(summary = "Добавление студента")
    public StudentDto addStudent(@RequestBody StudentDto studentDto) {
        aggregatorService.remoteLog("Добавлен студент", 3);
        return studentService.addStudent(studentDto);
    }

    @PutMapping
    @Operation(summary = "Изменение студента")
    public StudentDto updateStudent(@RequestBody StudentDto studentDto) {
        StudentDto student = studentService.updateStudent(studentDto);
        studentService.updateStudent(studentDto);
        aggregatorService.remoteLog("Обновлен студент", 3);
        return student;
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление студента по идентфиикатору")
    public void deleteStudentById(@PathVariable int id) {
        studentService.deleteStudentById(id);
        aggregatorService.remoteLog("Удален студент", 4);
    }

    @GetMapping("/teachers/{id}")
    @Operation(summary = "Получение всех учителей студента по идентификатору студента")
    public List<TeacherDto> getTeachersByStudentId(@PathVariable int id) {
        List<TeacherDto> allStudentTeachers = studentService.getTeachersByStudentId(id);
        aggregatorService.remoteLog("Получены все преподаватели по идентификатору студента", 2);
        return allStudentTeachers;
    }

    @PostMapping("/{studentId}/teacher/{teacherId}")
    @Operation(summary = "Приписание студента преподавателю")
    public void addStudentToTeacher(@PathVariable int studentId, @PathVariable int teacherId) {
        studentService.addStudentToTeacher(studentId, teacherId);
        aggregatorService.remoteLog("Студент с идентификатором: "+ studentId +" добавлен к преподавателю с идентификатором: " + teacherId, 3);
    }

    @DeleteMapping("/{studentId}/teacher/{teacherId}")
    @Operation(summary = "Отписание студента от преподавателя")
    public void removeStudentFromTeacher(@PathVariable int studentId, @PathVariable int teacherId) {
        studentService.removeStudentFromTeacher(studentId, teacherId);
        aggregatorService.remoteLog("Студент с идентификатором: "+ studentId +" отписан от преподавателя с идентификатором " + teacherId, 4);

    }
}
