package com.exam.examproject.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Schema(title = "Модель данных запроса на вход")
public class LoginRequestDto {

    @NotNull
    @Schema(title = "Логин")
    private String login;

    @NotNull
    @Schema(title = "Пароль")
    private String password;
}
