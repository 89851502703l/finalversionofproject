package com.exam.examproject.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

@Data
@Schema(title = "Модель данных роли")
public class Role implements GrantedAuthority {
    @Schema(title = "Идентификатор роли")
    private int roleId;
    @Schema(title = "Название роли")
    private String roleName;

    @Override
    public String getAuthority() {
        return roleName;
    }
}
