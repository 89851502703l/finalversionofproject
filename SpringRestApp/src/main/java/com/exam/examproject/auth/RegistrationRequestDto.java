package com.exam.examproject.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
@Schema(title = "Модель данных запроса на регистрацию")
public class RegistrationRequestDto {
    @NotNull
    @Schema(title = "Имя пользователя(логин)")
    private String username;

    @NotNull
    @Schema(title = "Пароль")
    private String password;

    @Schema(title = "Идентификатор роли")
    private int role_id;
}
