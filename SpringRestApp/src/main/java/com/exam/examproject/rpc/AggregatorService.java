package com.exam.examproject.rpc;

import org.exam.grpc.LogMessageSenderGrpc;
import org.exam.grpc.MessageRequest;
import org.exam.grpc.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AggregatorService {

    @Autowired
    private LogMessageSenderGrpc.LogMessageSenderBlockingStub stub;

    public void remoteLog(String logMassage, int logLevel){
        MessageRequest request = MessageRequest.newBuilder()
                .setLogMessage(logMassage)
                .setLevelOfLog(logLevel)
                .build();
        MessageResponse response = stub.logging(request);
        System.out.println(response);
    }

}