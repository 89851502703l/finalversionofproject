package com.exam.examproject.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(title = "Модель данных преподавателя")
public class TeacherDto {
    @Schema(title = "Идентификатор преподавателя")
    private int teacherId;
    @Schema(title = "Полное имя")
    private String fullName;
    @Schema(title = "Кафедра")
    private String chair;
}
