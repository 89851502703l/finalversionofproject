package com.exam.examproject.entity;

import lombok.Data;

@Data
public class TeacherEntity {
    private int teacherId;
    private String fullName;
    private String chair;
}
