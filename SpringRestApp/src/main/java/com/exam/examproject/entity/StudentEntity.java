package com.exam.examproject.entity;

import lombok.Data;

@Data
public class StudentEntity {
    private int studentId;
    private String fullName;
    private String specialty;
    private int course;
}
