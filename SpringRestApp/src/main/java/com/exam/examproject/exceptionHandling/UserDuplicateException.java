package com.exam.examproject.exceptionHandling;

public class UserDuplicateException extends RuntimeException {

    public UserDuplicateException (String message) {
        super(message);
    }
}
