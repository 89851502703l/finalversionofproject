package com.exam.examproject.servise.api;

import com.exam.examproject.dto.StudentDto;
import com.exam.examproject.dto.TeacherDto;

import java.util.List;

public interface StudentService {

    List<StudentDto> getAllStudents();

    StudentDto getStudentById(int id);

    StudentDto addStudent(StudentDto studentDto);

    StudentDto updateStudent(StudentDto studentDto);

    void deleteStudentById(int id);

    List<TeacherDto> getTeachersByStudentId(int id);

    void addStudentToTeacher(int studentId, int teacherId);

    void removeStudentFromTeacher(int studentId, int teacherId);

}
