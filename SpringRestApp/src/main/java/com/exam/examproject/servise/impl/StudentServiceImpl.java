package com.exam.examproject.servise.impl;

import com.exam.examproject.dao.StudentMapper;
import com.exam.examproject.dto.StudentDto;
import com.exam.examproject.dto.TeacherDto;
import com.exam.examproject.entity.StudentEntity;
import com.exam.examproject.entity.TeacherEntity;
import com.exam.examproject.servise.api.StudentService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private StudentMapper studentMapper;
    private ModelMapper modelMapper;

    @Autowired
    public StudentServiceImpl(StudentMapper studentMapper, ModelMapper modelMapper) {
        this.studentMapper = studentMapper;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<StudentDto> getAllStudents() {
        List<StudentEntity> entities = studentMapper.getAllStudents();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public StudentDto getStudentById(int id) {
        StudentEntity entity = studentMapper.getStudentById(id);
        return modelMapper.map(entity, StudentDto.class);
    }


    @Override
    public StudentDto addStudent(StudentDto studentDto) {
        StudentEntity entity = modelMapper.map(studentDto, StudentEntity.class);
        studentMapper.addStudent(entity);
        return getStudentById(entity.getStudentId());
    }

    @Override
    public StudentDto updateStudent(StudentDto studentDto) {
        StudentEntity student = modelMapper.map(studentDto, StudentEntity.class);
        studentMapper.updateStudent(student);
        return getStudentById(student.getStudentId());
    }

    @Override
    public void deleteStudentById(int id) {
        studentMapper.removeStudentFromAllTeachers(id);
        studentMapper.deleteStudentById(id);
    }

    @Override
    public List<TeacherDto> getTeachersByStudentId(int id) {
        List<TeacherEntity> entities = studentMapper.getTeachersByStudentId(id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public void addStudentToTeacher(int studentId, int teacherId) {
        studentMapper.addStudentToTeacher(studentId, teacherId);
    }

    @Override
    public void removeStudentFromTeacher(int studentId, int teacherId) {
        studentMapper.removeStudentFromTeacher(studentId, teacherId);
    }

}
