package com.exam.examproject.servise.impl;

import com.exam.examproject.dao.TeacherMapper;
import com.exam.examproject.dto.StudentDto;
import com.exam.examproject.dto.TeacherDto;
import com.exam.examproject.entity.StudentEntity;
import com.exam.examproject.entity.TeacherEntity;
import com.exam.examproject.servise.api.TeacherService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {

    private TeacherMapper teacherMapper;
    private ModelMapper modelMapper;

    @Autowired
    public TeacherServiceImpl(TeacherMapper teacherMapper, ModelMapper modelMapper) {
        this.teacherMapper = teacherMapper;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<TeacherDto> getAllTeachers() {
        List<TeacherEntity> entities = teacherMapper.getAllTeachers();
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

    @Override
    public TeacherDto getTeacherById(int id) {
        TeacherEntity entity = teacherMapper.getTeacherById(id);
        return modelMapper.map(entity, TeacherDto.class);
    }

    @Override
    public TeacherDto addTeacher(TeacherDto teacherDto) {
        TeacherEntity teacher = modelMapper.map(teacherDto, TeacherEntity.class);
        teacherMapper.addTeacher(teacher);
        return getTeacherById(teacher.getTeacherId());
    }

    @Override
    public TeacherDto updateTeacher(TeacherDto teacherDto) {
        TeacherEntity teacher = modelMapper.map(teacherDto, TeacherEntity.class);
        teacherMapper.updateTeacher(teacher);
        return getTeacherById(teacher.getTeacherId());
    }

    @Override
    public void deleteTeacher(int id) {
        teacherMapper.deleteTeacherFromAllStudents(id);
        teacherMapper.deleteTeacher(id);
    }

    @Override
    public List<StudentDto> getStudentsByTeacherId(int id) {
        List<StudentEntity> entities = teacherMapper.getStudentsByTeacherId(id);
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }
}
