package com.exam.examproject.servise.api;


import com.exam.examproject.dto.StudentDto;
import com.exam.examproject.dto.TeacherDto;

import java.util.List;

public interface TeacherService {

    List<TeacherDto> getAllTeachers();

    TeacherDto getTeacherById(int id);

    TeacherDto addTeacher(TeacherDto teacherDto);

    TeacherDto updateTeacher(TeacherDto teacher);

    void deleteTeacher(int id);

    List<StudentDto> getStudentsByTeacherId(int id);
}
