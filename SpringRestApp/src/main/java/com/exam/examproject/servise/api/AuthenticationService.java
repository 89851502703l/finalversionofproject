package com.exam.examproject.servise.api;


import com.exam.examproject.auth.LoginRequestDto;
import com.exam.examproject.auth.RegistrationRequestDto;
import com.exam.examproject.auth.User;
import org.springframework.security.core.Authentication;

public interface AuthenticationService {

    Authentication authorize(LoginRequestDto loginRequestDTO);

    User register(RegistrationRequestDto registrationRequestDTO);

    void logout();

}
