package com.exam.examproject.dao;

import com.exam.examproject.entity.StudentEntity;
import com.exam.examproject.entity.TeacherEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface StudentMapper {
    @Select("select * from students")
    List<StudentEntity> getAllStudents();

    @Select("select * from students where student_id = #{id}")
    StudentEntity getStudentById(int id);

    @Update("update students set full_name = #{fullName}, " +
            "specialty= #{specialty}, course = #{course} where student_id = #{studentId}")
    void updateStudent(StudentEntity student);

    @Insert("insert into students values (#{studentId}, #{fullName}, " +
            "#{specialty}, #{course})")
    @SelectKey(keyProperty = "studentId", before = true, resultType = Integer.class,
            statement = "select nextval('student_id_seq')")
    void addStudent(StudentEntity student);

    @Delete("delete from students where student_id = #{id}")
    void deleteStudentById(int id);

    @Select("select t.teacher_id, full_name, chair from teachers t\n" +
            " join students_teachers st on t.teacher_id = st.teacher_id where student_id = #{id}")
    List<TeacherEntity> getTeachersByStudentId(int id);

    @Insert("insert into students_teachers values (#{studentId}, #{teacherId})")
    void addStudentToTeacher(int studentId, int teacherId);

    @Delete("delete from students_teachers where student_id = #{studentId} AND " +
            "teacher_id = #{teacherId}")
    void removeStudentFromTeacher(int studentId, int teacherId);

    @Delete("delete from students_teachers where student_id = #{id}")
    void removeStudentFromAllTeachers(int id);
}
