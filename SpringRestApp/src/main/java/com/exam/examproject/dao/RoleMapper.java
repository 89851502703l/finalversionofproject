package com.exam.examproject.dao;

import com.exam.examproject.auth.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface RoleMapper {

    @Select("select * from authorities where authority_id = #{id}")
    Role getRoleById(int id);

}
