package com.exam.examproject.dao;

import com.exam.examproject.entity.StudentEntity;
import com.exam.examproject.entity.TeacherEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TeacherMapper {
    @Select("select * from teachers")
    List<TeacherEntity> getAllTeachers();

    @Select("select teacher_id, full_name, chair from teachers " +
            "where teacher_id = #{id}")
    TeacherEntity getTeacherById(int id);

    @Insert("insert into teachers values (#{teacherId}, #{fullName}, #{chair})")
    @SelectKey(keyProperty = "teacherId", before = true, resultType = Integer.class,
            statement = "select nextval('teacher_id_seq')")
    void addTeacher(TeacherEntity teacher);

    @Update("update teachers set full_name = #{fullName}," +
            "chair = #{chair} where teacher_id = #{teacherId}")
    void updateTeacher(TeacherEntity teacher);

    @Delete("delete from teachers where teacher_id = #{id}")
    void deleteTeacher(int id);

    @Select("select s.student_id, full_name, specialty, course from students s " +
            "join students_teachers st on s.student_id = st.student_id " +
            "where teacher_id = #{id}")
    List<StudentEntity> getStudentsByTeacherId(int id);

    @Delete("delete from students_teachers where teacher_id = #{id}")
    void deleteTeacherFromAllStudents(int id);
}
