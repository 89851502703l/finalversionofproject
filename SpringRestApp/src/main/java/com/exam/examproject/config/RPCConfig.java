package com.exam.examproject.config;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.exam.grpc.LogMessageSenderGrpc;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RPCConfig {
    @Bean
    public LogMessageSenderGrpc.LogMessageSenderBlockingStub logMessageSenderBlockingStub(){
        ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:6565").usePlaintext().build();
        return LogMessageSenderGrpc.newBlockingStub(channel);
    }
}
