create sequence if not exists student_id_seq start 1;

CREATE TABLE IF NOT EXISTS students
(
    student_id INTEGER DEFAULT nextval('student_id_seq') PRIMARY KEY,
    full_name  VARCHAR(100),
    specialty  VARCHAR(50)                                           NOT NULL,
    course     INTEGER                                               NOT NULL check (course BETWEEN 1 AND 6)
);

create sequence if not exists teacher_id_seq start 1;

CREATE TABLE IF NOT EXISTS teachers
(
    teacher_id INTEGER DEFAULT nextval('teacher_id_seq') PRIMARY KEY NOT NULL,
    full_name  VARCHAR(100),
    chair      VARCHAR                                               NOT NULL
);

CREATE TABLE IF NOT EXISTS students_teachers
(
    student_id INTEGER references students (student_id),
    teacher_id INTEGER references teachers (teacher_id)
);

create sequence if not exists authority_id_seq start 1;

CREATE TABLE IF NOT EXISTS authorities
(
    authority_id INTEGER default nextval('authority_id_seq') PRIMARY KEY ,
    role_name    VARCHAR(50)                            NOT NULL UNIQUE
);

create sequence if not exists user_id_seq start 1;

CREATE TABLE IF NOT EXISTS users
(
    user_id  INTEGER default nextval('user_id_seq') PRIMARY KEY ,
    username VARCHAR(50)                            NOT NULL UNIQUE,
    password VARCHAR(150)                           NOT NULL
);

CREATE TABLE IF NOT EXISTS users_roles
(
    user_id int not null references users (user_id),
    role_id int not null references authorities (authority_id)
);
