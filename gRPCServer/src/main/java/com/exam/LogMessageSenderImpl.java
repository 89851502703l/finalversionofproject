package com.exam;



import io.grpc.stub.StreamObserver;
import org.exam.grpc.LogMessageSenderGrpc;
import org.exam.grpc.MessageRequest;
import org.exam.grpc.MessageResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogMessageSenderImpl extends LogMessageSenderGrpc.LogMessageSenderImplBase {
    @Override
    public void logging(MessageRequest request, StreamObserver<MessageResponse> responseObserver) {
        Logger logger = LoggerFactory.getLogger(LogMessageSenderImpl.class);
        int logLevel = request.getLevelOfLog();
        switch (logLevel) {
            case 1:
                logger.trace(request.getLogMessage());
                break;
            case 2:
                logger.debug(request.getLogMessage());
                break;
            case 3:
                logger.info(request.getLogMessage());
                break;
            case 4:
                logger.warn(request.getLogMessage());
                break;
            case 5:
                logger.error(request.getLogMessage());
                break;
            default:
                System.out.println("Level of log is wrong :(");
        }
        if(logLevel > 0 && logLevel < 6) {
            MessageResponse response = MessageResponse.newBuilder().setStatus("Your message was logged with level: "
                    + logLevel).build();
            responseObserver.onNext(response);
        } else {
            MessageResponse response = MessageResponse.newBuilder().setStatus("Your message wasn't log, enter correct level log (0<log<6)").build();
            responseObserver.onNext(response);
        }

        responseObserver.onCompleted();
    }
}
