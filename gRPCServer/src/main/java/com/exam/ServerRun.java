package com.exam;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

/**
 * Hello world!
 *
 */

public class ServerRun {
    public static void main( String[] args ) throws IOException, InterruptedException {
        Server server = ServerBuilder
                .forPort(6565)
                .addService(new LogMessageSenderImpl()).build();

        server.start();
        System.out.println("Server started");
        server.awaitTermination();
        System.out.println("Server stopped");
    }
}
