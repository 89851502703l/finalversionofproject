Переписал проект в модулях чтобы нагляднее показать работу RPC.

proto-module: модуль с proto файлом и сгенерированными из него классами(передаем его через maven зависимость).
gRPCServer: содержит в себе класс сервера и класс реализующий функцию логирования с ответным статусом(для примера).
SpringRestApp: то же самое web приложение + реализация клиента RPC (RPCConfig, AggregatorService) + демонстрация работы (в StudentController добавлены удаленные вызовы функции)

PS: Поздно загружаю проект из-за работы в оба дня.